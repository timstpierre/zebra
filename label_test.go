package zebra

import (
	"testing"
)

func TestLabel(test *testing.T) {
	label := New(50, 75, 8)
	label.TextLine(20, 20, "C", 36, "This is a test")
	//label.Ellipse(0, 0, 400, 200, 4, false)
	//	label.Circle(0, 0, 100, 8, false)
	label.Code39(20, 80, 40, "00085D123456", "N", true, false)
	label.Print("172.17.16.161")

}
