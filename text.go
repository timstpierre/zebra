package zebra

import (
	"strconv"
)

func (label *Label) TextLine(x int, y int, font string, size int, text string) {
	var fontstr string

	if size > 0 {
		fontstr = "^A" + font + "N," + strconv.Itoa(size)
	} else {
		fontstr = "^A" + font + "N"
	}

	command := "^FO" + strconv.Itoa(label.MinX+x) + "," + strconv.Itoa(label.MinY+y) + fontstr + "^FD" + text + "^FS"
	label.Data = append(label.Data, command)
}

func (label *Label) Code39(x int, y int, height int, data string, orientation string, interpretation bool, check bool) {
	var checkstr string
	var interpretstr string

	if interpretation {
		interpretstr = ",Y"
	} else {
		interpretstr = ",N"
	}

	if check {
		checkstr = ",Y"
	} else {
		checkstr = ",N"
	}
	command := "^FO" + strconv.Itoa(label.MinX+x) + "," + strconv.Itoa(label.MinY+y) + "^B3" + orientation + checkstr + "," + strconv.Itoa(height) + interpretstr + "^FD" + data + "^FS"
	label.Data = append(label.Data, command)
}

func (label *Label) Code128(x int, y int, height int, data string, orientation string, interpretation bool, check bool) {
	var checkstr string
	var interpretstr string

	if interpretation {
		interpretstr = ",Y,N"
	} else {
		interpretstr = ",N,N"
	}

	if check {
		checkstr = ",Y"
	} else {
		checkstr = ",N"
	}
	command := "^FO" + strconv.Itoa(label.MinX+x) + "," + strconv.Itoa(label.MinY+y) + "^BC" + orientation + "," + strconv.Itoa(height) + interpretstr + checkstr + "^FD" + data + "^FS"
	label.Data = append(label.Data, command)
}
