package zebra

import (
	"strconv"
)

func (label *Label) Box(x int, y int, width int, height int, thickness int, invert bool, round int) {
	var colour string
	if invert {
		colour = "W"
	} else {
		colour = "B"
	}
	if round > 8 {
		round = 8
	}
	if (x + width + label.MinX) > label.MaxWidth {
		width = label.MinX + label.MaxWidth - x - 10
	}
	if (y + height + label.MinY) > label.MaxHeight {
		height = label.MinY + label.MaxHeight - y - 10
	}

	command := "^FO" + strconv.Itoa(label.MinX+x) + "," + strconv.Itoa(label.MinY+y) + "^GB" + strconv.Itoa(width) + "," + strconv.Itoa(height) + "," + strconv.Itoa(thickness) + "," + colour + "," + strconv.Itoa(round) + "^FS"
	label.Data = append(label.Data, command)
}

func (label *Label) Circle(x int, y int, diameter int, thickness int, invert bool) {
	var colour string
	if invert {
		colour = "W"
	} else {
		colour = "B"
	}

	command := "^FO" + strconv.Itoa(label.MinX+x) + "," + strconv.Itoa(label.MinY+y) + "^GC" + strconv.Itoa(diameter) + "," + strconv.Itoa(thickness) + "," + colour + "^FS"
	label.Data = append(label.Data, command)
}

func (label *Label) Ellipse(x int, y int, width int, height int, thickness int, invert bool) {
	var colour string
	if invert {
		colour = "W"
	} else {
		colour = "B"
	}

	if (x + width + label.MinX) > label.MaxWidth {
		width = label.MinX + label.MaxWidth - x - 10
	}
	if (y + height + label.MinY) > label.MaxHeight {
		height = label.MinY + label.MaxHeight - y - 10
	}

	command := "^FO" + strconv.Itoa(label.MinX+x) + "," + strconv.Itoa(label.MinY+y) + "^GE" + strconv.Itoa(width) + "," + strconv.Itoa(height) + "," + strconv.Itoa(thickness) + "," + colour + "," + "^FS"
	label.Data = append(label.Data, command)
}
