package zebra

import (
	log "github.com/sirupsen/logrus"
	"net"
)

type Label struct {
	Height    int
	Width     int
	Data      []string
	DP        int
	MinX      int
	MinY      int
	MaxWidth  int
	MaxHeight int
}

func New(height int, width int, dp int) Label {
	xorigin := (812/dp - 75) / 2 * dp
	label := Label{
		Height:    height,
		Width:     width,
		DP:        dp,
		MaxHeight: height * dp,
		MaxWidth:  width*dp - xorigin - 5,
		MinX:      xorigin + 10,
		MinY:      10,
	}
	log.Infof("X Y offset is %v, %v", label.MinX, label.MinY)
	return label
}

func (label *Label) Print(host string) error {
	con, err := net.Dial("tcp", host+":9100")
	if err != nil {
		log.Errorf("Problem connecting to printer %v", err)
		return err
	}
	con.Write([]byte("^XA"))
	for _, line := range label.Data {
		con.Write([]byte(line + "\n"))
		log.Info(line)
	}
	con.Write([]byte("^XZ"))
	con.Close()
	return nil
}
